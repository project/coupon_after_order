<?php

namespace Drupal\coupon_after_order\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Coupon After Order settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Settings form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'coupon_after_order_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['coupon_after_order.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if ($this->moduleHandler->moduleExists('token')) {
      $form['token_caption'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Replacement patterns'),
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'commerce_order',
          'commerce_promotion',
          'commerce_promotion_coupon',
        ],
      ];
    }

    $form['generate_transition'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Generate coupon at transition'),
      '#description' => $this->t('Leave empty if you want to call this logic with your own EventSubscriber. For example when you want to generate coupons and send it to the customer in the order invoice mail.'),
      '#default_value' => $this->config('coupon_after_order.settings')->get('generate_transition') ?? 'place',
    ];

    $form['send_email_after_generating'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send e-mail after generating'),
      '#description' => $this->t('It sends the coupon via a separate e-mail with the provided subject and text. Uncheck if you want to call this logic with your own EventSubscriber. For example when you want to generate coupons and send it to the customer in the order invoice mail.'),
      '#default_value' => $this->config('coupon_after_order.settings')->get('send_email_after_generating') ?? TRUE,
    ];

    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail subject'),
      '#default_value' => $this->config('coupon_after_order.settings')->get('email_subject'),
    ];

    $form['email_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('E-mail text'),
      '#default_value' => $this->config('coupon_after_order.settings')->get('email_text'),
      '#rows' => 15,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('coupon_after_order.settings')
      ->set('generate_transition', $form_state->getValue('generate_transition'))
      ->set('send_email_after_generating', $form_state->getValue('send_email_after_generating'))
      ->set('email_subject', $form_state->getValue('email_subject'))
      ->set('email_text', $form_state->getValue('email_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

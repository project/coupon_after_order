<?php

namespace Drupal\coupon_after_order\Controller;

use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_promotion\CouponCodeGeneratorInterface;
use Drupal\commerce_promotion\CouponCodePattern;
use Drupal\commerce_promotion\Entity\CouponInterface;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\coupon_after_order\Event\CouponAfterOrderEvents;
use Drupal\coupon_after_order\Event\CouponBeforeCreateAfterOrderEvent;
use Drupal\coupon_after_order\Event\CouponCreatedAfterOrderEvent;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Utility\Token;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Sets coupons after placing the order.
 */
class CouponAfterOrderController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The language default.
   *
   * @var \Drupal\Core\Language\LanguageDefault
   */
  protected $languageDefault;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $couponAfterOrderConfig;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Promotion storage.
   *
   * @var \Drupal\commerce_promotion\PromotionStorageInterface
   */
  protected $promotionStorage;

  /**
   * Promotion coupon storage.
   *
   * @var \Drupal\commerce_promotion\CouponStorageInterface
   */
  protected $promotionCouponStorage;

  /**
   * Coupon code generator.
   *
   * @var \Drupal\commerce_promotion\CouponCodeGeneratorInterface
   */
  protected $couponCodeGenerator;

  /**
   * Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Token service.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Language\LanguageDefault $language_default
   *   The language default.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\commerce_promotion\CouponCodeGeneratorInterface $coupon_code_generator
   *   Coupon code generator.
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   * @param \Drupal\commerce\MailHandlerInterface $mail_handler
   *   Commerce mail handler.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EventDispatcherInterface $event_dispatcher,
    LanguageDefault $language_default,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    CouponCodeGeneratorInterface $coupon_code_generator,
    Token $token,
    MailHandlerInterface $mail_handler
  ) {
    $this->languageDefault = $language_default;
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->eventDispatcher = $event_dispatcher;
    $this->couponAfterOrderConfig = $this->configFactory->get('coupon_after_order.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->promotionStorage = $this->entityTypeManager->getStorage('commerce_promotion');
    $this->promotionCouponStorage = $this->entityTypeManager->getStorage('commerce_promotion_coupon');
    $this->couponCodeGenerator = $coupon_code_generator;
    $this->token = $token;
    $this->mailHandler = $mail_handler;
  }

  /**
   * Generates coupon codes if viable.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return array
   *   If nothing was created, empty array.
   *   If coupon creation was successful, associative array with 2 keys:
   *   - promotion,
   *   - coupon.
   */
  public function generateCoupons(OrderInterface $order) {
    // Also check currency.
    $currency = $order->getTotalPrice()->getCurrencyCode();

    // Get the first enabled promotion that should use coupons and its
    // coupon generation is enabled.
    $query = $this->promotionStorage->getQuery('AND');

    $priceOrCondition = $query->orConditionGroup()
      ->condition('coupon_after_order_min_price__number', $order->getTotalPrice()
        ->getNumber(), '<=')
      ->notExists('coupon_after_order_min_price__number');

    $currencyOrCondition = $query->orConditionGroup()
      ->condition('coupon_after_order_min_price__currency_code', $currency)
      ->notExists('coupon_after_order_min_price__currency_code');

    $orderType = $order->bundle();
    $orderTypeCondition = $query->orConditionGroup()
      ->condition('order_types', $orderType, 'IN')
      ->notExists('order_types');

    $store = $order->getStore();
    $storeId = (isset($store)) ? $store->id() : NULL;
    $storeTimezone = (isset($store)) ? $store->getTimezone() : NULL;
    $storeTimezone = ($storeTimezone) ?? 'UTC';

    if (isset($store)) {
      $storeCondition = $query->orConditionGroup()
        ->condition('stores', $storeId, 'IN')
        ->notExists('stores');
    }

    $currentTimeDateTime = new DrupalDateTime('now', $storeTimezone);
    $currentTime = $currentTimeDateTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $startDateOrCondition = $query->orConditionGroup()
      ->condition('start_date', $currentTime, '<=')
      ->notExists('start_date');

    $endDateOrCondition = $query->orConditionGroup()
      ->condition('end_date', $currentTime, '>=')
      ->notExists('end_date');

    $promotionQuery = $query
      ->condition('require_coupon', 1)
      ->condition('coupon_after_order', 1)
      ->condition('status', 1)
      ->condition($startDateOrCondition)
      ->condition($endDateOrCondition)
      ->condition($priceOrCondition)
      ->condition($currencyOrCondition)
      ->condition($orderTypeCondition);

    if (isset($store)) {
      $promotionQuery = $promotionQuery->condition($storeCondition);
    }

    $promotions = $promotionQuery
      ->range(0, 1)
      ->sort('weight', 'ASC')
      ->sort('promotion_id', 'ASC')
      ->accessCheck(FALSE)
      ->execute();

    $promotion = reset($promotions);

    // If there's no promotion defined to generate coupons, exit.
    if (empty($promotion)) {
      return [];
    }

    $promotion = $this->promotionStorage->load($promotion);
    if (empty($promotion)) {
      return [];
    }

    $pattern = new CouponCodePattern(CouponCodePattern::ALPHANUMERIC);
    $codes = [];
    $tried = 0;

    while ($tried < 20 && empty($codes)) {
      $codes = $this->couponCodeGenerator->generateCodes($pattern, 10);
      $tried += 1;
    }

    $code = reset($codes);
    if (empty($code)) {
      return [];
    }

    $coupon = $this->promotionCouponStorage->create([
      'code' => $code,
      'promotion_id' => $promotion->id(),
      'usage_limit' => 1,
      'usage_limit_customer' => NULL,
      'status' => 1,
      'start_date' => $currentTime,
    ]);

    $eventBeforeCreate = new CouponBeforeCreateAfterOrderEvent($order, $promotion, $coupon);

    // \Symfony\Component\HttpKernel\Kernel::VERSION will give the symfony
    // version. However, testing this does not give the required outcome, we
    // need to test the Drupal core version.
    // @todo Remove the check when Core 9.1 is the lowest supported version.
    if (version_compare(\Drupal::VERSION, '9.1', '>=')) {
      // The new way, with $event first.
      $this->eventDispatcher->dispatch($eventBeforeCreate, CouponAfterOrderEvents::COUPON_BEFORE_CREATE);
    }
    else {
      // Replicate the existing dispatch signature.
      $this->eventDispatcher->dispatch(CouponAfterOrderEvents::COUPON_BEFORE_CREATE, $eventBeforeCreate);
    }

    $coupon->save();

    $event = new CouponCreatedAfterOrderEvent($order, $promotion, $coupon);

    // \Symfony\Component\HttpKernel\Kernel::VERSION will give the symfony
    // version. However, testing this does not give the required outcome, we
    // need to test the Drupal core version.
    // @todo Remove the check when Core 9.1 is the lowest supported version.
    if (version_compare(\Drupal::VERSION, '9.1', '>=')) {
      // The new way, with $event first.
      $this->eventDispatcher->dispatch($event, CouponAfterOrderEvents::COUPON_CREATED);
    }
    else {
      // Replicate the existing dispatch signature.
      $this->eventDispatcher->dispatch(CouponAfterOrderEvents::COUPON_CREATED, $event);
    }

    return [
      'promotion' => $promotion,
      'coupon' => $coupon,
    ];
  }

  /**
   * Sends the coupons via e-mail.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order object.
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   Promotion.
   * @param \Drupal\commerce_promotion\Entity\CouponInterface $coupon
   *   Coupon.
   *
   * @returns bool
   *   Mail send result.
   */
  public function sendCoupons(OrderInterface $order, PromotionInterface $promotion, CouponInterface $coupon) {
    $langcode = NULL;
    $customer = $order->getCustomer();
    if ($customer->isAuthenticated()) {
      $langcode = $customer->getPreferredLangcode();
    }

    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
    }

    $currentLanguageId = $this->languageManager->getCurrentLanguage()->getId();
    $this->changeActiveLanguage($langcode);

    // Update used config language by reloading config.
    $this->couponAfterOrderConfig = $this->configFactory->get('coupon_after_order.settings');

    $to = $order->getEmail();
    $subject = $this->couponAfterOrderConfig->get('email_subject') ?? 'You get discount on your next purchase | [commerce_order:store_id:entity:name]';
    $body = $this->couponAfterOrderConfig->get('email_text') ??
      '<p>Dear customer!</p>\r\n\r\n<p>[commerce_promotion:description]</p>\r\n\r\n<h2>[commerce_promotion_coupon:code]</h2>\r\n\r\n<p>Choose products on <a href=\"[site:url]\" target=\"_blank\">[site:name]</a>,<br>\r\nadd them to your cart and enter the coupon code at checkout.</p>\r\n\r\n<p><a href=\"[site:url]\" target=\"_blank\">VIEW OFFERS</a></p>\r\n\r\n<p>Thank you for your business, hope to deal with you again.</p>\r\n\r\n<p>[commerce_order:store_id:entity:name]</p>';

    $subjectTokenized = $this->token->replace(
      $subject,
      [
        'commerce_promotion' => $promotion,
        'commerce_promotion_coupon' => $coupon,
        'commerce_order' => $order,
      ],
      [
        'langcode' => $langcode,
      ]
    );

    $bodyTokenized = $this->token->replace(
      $body,
      [
        'commerce_promotion' => $promotion,
        'commerce_promotion_coupon' => $coupon,
        'commerce_order' => $order,
      ],
      [
        'langcode' => $langcode,
      ]
    );

    $bodyTokenized = [
      '#type' => 'inline_template',
      '#template' => $bodyTokenized,
      '#context' => [],
    ];

    $params = [
      'langcode' => $langcode,
    ];

    $this->changeActiveLanguage($currentLanguageId);

    // Reset used config language by reloading config.
    $this->couponAfterOrderConfig = $this->configFactory->get('coupon_after_order.settings');

    return $this->mailHandler->sendMail($to, $subjectTokenized, $bodyTokenized, $params);
  }

  /**
   * Changes the active language for translations.
   *
   * Taken from Drupal\commerce\MailHandler because there it is a protected
   * method.
   *
   * @param string $langcode
   *   The langcode.
   */
  protected function changeActiveLanguage($langcode) {
    if (!$this->languageManager->isMultilingual()) {
      return;
    }
    $language = $this->languageManager->getLanguage($langcode);
    if (!$language) {
      return;
    }
    // The language manager has no method for overriding the default
    // language, like it does for config overrides. We have to change the
    // default language service's current language.
    // @see https://www.drupal.org/project/drupal/issues/3029010
    $this->languageDefault->set($language);
    $this->languageManager->setConfigOverrideLanguage($language);
    $this->languageManager->reset();

    // The default string_translation service, TranslationManager, has a
    // setDefaultLangcode method. However, this method is not present on
    // either of its interfaces. Therefore we check for the concrete class
    // here so that any swapped service does not break the application.
    // @see https://www.drupal.org/project/drupal/issues/3029003
    $string_translation = $this->getStringTranslation();
    if ($string_translation instanceof TranslationManager) {
      $string_translation->setDefaultLangcode($language->getId());
      $string_translation->reset();
    }
  }

}

<?php

namespace Drupal\coupon_after_order\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\coupon_after_order\Controller\CouponAfterOrderController;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Coupon After Order event subscriber.
 */
class CouponAfterOrderSubscriber implements EventSubscriberInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $couponAfterOrderConfig;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Coupon After Order Controller.
   *
   * @var \Drupal\coupon_after_order\Controller\CouponAfterOrderController
   */
  protected $couponAfterOrderController;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\coupon_after_order\Controller\CouponAfterOrderController $couponAfterOrderController
   *   Coupon After Order Controller.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(
    CouponAfterOrderController $couponAfterOrderController,
    EventDispatcherInterface $event_dispatcher,
    ConfigFactoryInterface $config_factory
  ) {
    $this->couponAfterOrderController = $couponAfterOrderController;
    $this->eventDispatcher = $event_dispatcher;
    $this->configFactory = $config_factory;
    $this->couponAfterOrderConfig = $this->configFactory->get('coupon_after_order.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Subscribe to kernel request very early.
    $events[KernelEvents::REQUEST][] = ['onRequest', 900];
    return $events;
  }

  /**
   * Attaches the proper transition event for generating coupons.
   */
  public function onRequest() {
    $transition = $this->couponAfterOrderConfig->get('generate_transition') ?? '';

    if (!empty($transition)) {
      // Make sure to run after OrderReceiptSubscriber.
      // So, first the invoice is sent and then the offer with the coupon.
      $this->eventDispatcher->addListener(
        "commerce_order.{$transition}.post_transition",
        [$this, 'onOrderTransition'],
        -101
      );
    }
  }

  /**
   * Generates coupon code on the specified transition.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Transition event.
   */
  public function onOrderTransition(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();

    if (!$order instanceof OrderInterface) {
      return;
    }

    // Generate coupon.
    $coupons = $this->couponAfterOrderController->generateCoupons($order);

    // Send an e-mail with the coupon if it was created and mail enabled.
    if (!empty($coupons['coupon']) &&
      !empty($coupons['promotion']) &&
      $this->couponAfterOrderConfig->get('send_email_after_generating')
    ) {
      $this->couponAfterOrderController->sendCoupons(
        $order,
        $coupons['promotion'],
        $coupons['coupon']
      );
    }

  }

}

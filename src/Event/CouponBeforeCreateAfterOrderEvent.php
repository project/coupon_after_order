<?php

namespace Drupal\coupon_after_order\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_promotion\Entity\CouponInterface;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that fires before saving the automatically generated coupon.
 */
class CouponBeforeCreateAfterOrderEvent extends Event {

  /**
   * Order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Promotion.
   *
   * @var \Drupal\commerce_promotion\Entity\PromotionInterface
   */
  protected $promotion;

  /**
   * Coupon to create.
   *
   * @var \Drupal\commerce_promotion\Entity\CouponInterface
   */
  protected $coupon;

  /**
   * InvoiceCreatedEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order object.
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   Promotion.
   * @param \Drupal\commerce_promotion\Entity\CouponInterface $coupon
   *   Coupon.
   */
  public function __construct(OrderInterface $order, PromotionInterface $promotion, CouponInterface $coupon) {
    $this->order = $order;
    $this->promotion = $promotion;
    $this->coupon = $coupon;
  }

  /**
   * Gets order.
   *
   * @return \Drupal\commerce_order\Entity\Order
   *   Order for which event is fired.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Returns promotion.
   *
   * @return \Drupal\commerce_promotion\Entity\PromotionInterface
   *   Promotion.
   */
  public function getPromotion(): PromotionInterface {
    return $this->promotion;
  }

  /**
   * Returns coupon.
   *
   * @return \Drupal\commerce_promotion\Entity\CouponInterface
   *   Coupon.
   */
  public function getCoupon(): CouponInterface {
    return $this->coupon;
  }

  /**
   * Sets coupon data. Could be used for altering automatic coupon creation.
   *
   * @param \Drupal\commerce_promotion\Entity\CouponInterface $coupon
   *   New coupon data.
   */
  public function setCoupon(CouponInterface $coupon): void {
    $this->coupon = $coupon;
  }

}

<?php

namespace Drupal\coupon_after_order\Event;

/**
 * Defines events for the coupon_after_order module.
 */
final class CouponAfterOrderEvents {

  /**
   * Name of event fired before the coupon was automatically generated.
   *
   * @Event
   *
   * @see \Drupal\coupon_after_order\Event\CouponBeforeCreateAfterOrderEvent
   */
  const COUPON_BEFORE_CREATE = 'coupon_after_order.coupon_before_create';

  /**
   * Name of event fired after coupon was automatically generated for the order.
   *
   * @Event
   *
   * @see \Drupal\coupon_after_order\Event\CouponCreatedAfterOrderEvent
   */
  const COUPON_CREATED = 'coupon_after_order.coupon_created';

}

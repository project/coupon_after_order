# Coupon After Order module

Creates coupons after the order is placed and sends them to the customers
via e-mail.

## Usage

### Setting up when the e-mail is sent
1. Enable the module and go to its settings page: 
`/admin/commerce/config/coupon_after_order`
2. By default, the "place" transition is set. That means the 
   coupon generating code runs when the order is placed. Change it 
   to the proper transition. These transitions are defined in 
   `<modulename>.workflows.yml` files. By default commerce has these: 
   `commerce/modules/order/commerce_order.workflows.yml`. You can set
    whatever transition from the order workflow you are using currently.
3. Set the other fields on this form. This is saved to the site configuration.
   The e-mail subject and test are translatable. You can also use field values 
   as tokens from order, promotion and coupon entities.
   The language detection for the e-amils is the same what's in commerce. 
   If anonymous, the current site language is used. If authenticated, 
   the preferred_langcode field value is used.
   If you want to include different promotion description 
   for different languages,first set the Promotion entity to be translatable at
  `/admin/config/regional/content-language`
  (you'll need the core language modules for this).
   Then, create translations for the Promotions the same way as for products,
    nodes or taxonomy terms.

### Enabling coupon creation
4. Coupon is created for the first enabled promotion that has the
   "Require a coupon to apply this promotion", "Create coupon after order"
   fields enabled (and optionally a minimal full price is set). Only the first
   promotion is used.
   Reminder: you can set the order of promotions by weight at page
   `/admin/commerce/promotions/reorder`.

## Services and events for other modules
If you develop a custom solution, you can use from this module:
- `coupon_after_order.coupon_before_create` Event
  This event fires before saving the newly generated coupon by this module.
  You can alter the coupon data in your EventSubscriber like this:
  $event->setCoupon($coupon);
- `coupon_after_order.coupon_created` Event
  This event fires when a successful coupon creation has
  happened by this module.
- `coupon_after_order.controller` service
  From here you can call this module's coupon generating and e-mail
  sending logic manually.
